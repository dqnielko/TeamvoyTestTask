package test.task;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import test.task.dto.ItemDto;
import test.task.dto.OrderDto;
import test.task.entity.Item;

import static org.junit.Assert.assertEquals;

public class ControllersTest extends AbstractTest{
    private final int numberOfItemsForTestCreation = 3;
    private final String testItemName="cookie";
    @Override
    @Before
    public void setUp(){
        super.setUp();
    }
    @Test
    public void itemCreation() throws Exception {
        String uri="/item";
        ItemDto itemDto = ItemDto.builder().name("onion").price(250).build();
        String itemJson = super.mapToJson(itemDto);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(itemJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(status,200);
    }
    public void addItemsToDb(String name, double price, int number)throws Exception{
        String uriForItemCreation = "/item";
        for(int i =0;i<number;i++){
            ItemDto itemDto = ItemDto.builder().name(name).price(price).build();
            String itemJson = super.mapToJson(itemDto);
            mvc.perform(MockMvcRequestBuilders.post(uriForItemCreation)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(itemJson)).andReturn();
        }
    }
    public Item[] requestItemsFromDbWithSmallestPrice(String name, int number)throws Exception{
        String uri = "/item/"+name+"/"+number;
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        return super.mapFromJson(result,Item[].class);
    }
    @Test
    public void itemsPopping()throws Exception{
        //creating items for popping from db
        addItemsToDb(testItemName,100,numberOfItemsForTestCreation);
        addItemsToDb(testItemName,200,numberOfItemsForTestCreation);
        //popping all items from db with the smallest price
        Item[] items = requestItemsFromDbWithSmallestPrice(testItemName,numberOfItemsForTestCreation);
        assertEquals(items.length,numberOfItemsForTestCreation);
        for (Item item : items) {
            assertEquals(item.getName(), testItemName);
            assertEquals(0, Double.compare(item.getPrice(), 100.0));
        }
        //popping items from db that are left and currently have the smallest price in storage
        items = requestItemsFromDbWithSmallestPrice(testItemName,numberOfItemsForTestCreation);
        assertEquals(items.length,numberOfItemsForTestCreation);
        for (Item item : items) {
            assertEquals(item.getName(), testItemName);
            assertEquals(0, Double.compare(item.getPrice(), 200.0));
        }
        //trying to pop once more, but there must be no items like that in that case
        items = requestItemsFromDbWithSmallestPrice(testItemName,numberOfItemsForTestCreation);
        assertEquals(items.length,0);
    }
    @Test
    public void createOrder()throws Exception{
        String uri = "/order";
        //adding item for testing
        addItemsToDb(testItemName,100,1);
        OrderDto orderDto = OrderDto.builder().itemName(testItemName).quantity(10)
                .build();
        String itemJson = super.mapToJson(orderDto);
        //sending post request for order creation
        MvcResult mvcResult =  mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(itemJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(status, HttpStatus.OK.value());
    }
}
