package test.task.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name="items")
public class Item {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;
    @Column
    private double price;
}
