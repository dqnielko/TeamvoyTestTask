package test.task.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private double price;
    @Column
    private int quantity;
    @Column
    private Date creationDate;
    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name="item_id")
    private Item item;
}
