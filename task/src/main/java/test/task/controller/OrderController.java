package test.task.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import test.task.dto.OrderDto;
import test.task.service.OrderService;

@RestController
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;
    @PostMapping("order")
    public ResponseEntity<String> createOrder(@RequestBody OrderDto orderDto){
        ResponseEntity<String> responseEntity = ResponseEntity.badRequest().build();
        if(orderService.createOrder(orderDto)){
            responseEntity = ResponseEntity.ok().build();
        }
        return responseEntity;
    }
}
