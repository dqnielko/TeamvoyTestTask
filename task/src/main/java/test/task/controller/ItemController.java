package test.task.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import test.task.dto.ItemDto;
import test.task.entity.Item;
import test.task.service.ItemService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ItemController {
    private final ItemService itemService;
    @PostMapping("item")
    public ResponseEntity<String> addItem(@RequestBody ItemDto itemDto){
        itemService.addItem(itemDto);
        return ResponseEntity.ok().build();
    }
    @GetMapping("item/{name}/{limit}")
    public ResponseEntity<List<Item>> popItemsWithMinPrice(@PathVariable String name,
                                                           @PathVariable int limit){
        List<Item> items = itemService.
                popItemsByNameAndItsLowestPriceLimitedByNumber(name,limit);
        return  ResponseEntity.ok(items);
    }
}
