package test.task.scheduler;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import test.task.service.OrderService;

@Component
public class OrderScheduler {
    private final OrderService orderService;
    public OrderScheduler(OrderService orderService){
        this.orderService = orderService;
    }
    @Scheduled(fixedDelayString = "${expiration_milliseconds}")
    public void deleteExpiredOrders(){
        orderService.deleteExpiredOrders();
        System.out.println("Deleted expired orders");
    }
}
