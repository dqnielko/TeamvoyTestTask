package test.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import test.task.entity.Order;

import java.util.Date;

public interface OrderRepository extends JpaRepository<Order,Long> {
    @Modifying
    @Query("delete from Order o where o.creationDate not between  :startDate and :endDate")
    void deleteAllByCreationDateNotBetween(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
