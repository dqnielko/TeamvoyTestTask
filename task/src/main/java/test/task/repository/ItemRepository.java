package test.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import test.task.entity.Item;

import java.util.List;
import java.util.Optional;

public interface ItemRepository extends JpaRepository<Item,Long> {
    Optional<Item> findByName(String name);
    @Query("select min(i.price) from Item i where i.name=:name")
    Optional<Double> findMinPriceWhereName(@Param("name") String name);
    @Query(value = "select * from items where name=:name and price=:price limit :limit",nativeQuery = true)
    List<Item> findItemsByNameAndPriceLimitedByNumber(@Param("name")String name,
                                                      @Param("price")double price,
                                                      @Param("limit")int limit);
    @Query(value="delete from items where name=:name and price=:price limit :limit",nativeQuery = true)
    @Modifying
    void deleteItemsByNameAndPriceLimitedByNumber(@Param("name") String name,
                                                  @Param("price") double price,
                                                  @Param("limit") int limit);
}
