package test.task.service;

import lombok.RequiredArgsConstructor;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import test.task.dto.OrderDto;
import test.task.entity.Item;
import test.task.entity.Order;
import test.task.repository.OrderRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderService {
    @Value("${expiration_minutes}")
    private int expirationMinutes;
    private final ItemService itemService;
    private final OrderRepository orderRepository;
    public boolean createOrder(OrderDto orderDto){
        boolean result = false;
        Optional<Item> item = itemService.findByName(orderDto.getItemName());
        if(item.isPresent()){
            Order order = Order.builder().item(item.get())
                    .price(item.get().getPrice()*orderDto.getQuantity())
                    .quantity(orderDto.getQuantity())
                    .creationDate(new Date())
                    .build();
            orderRepository.save(order);
            result = true;
        }
        return result;
    }
    @Transactional
    public void deleteExpiredOrders(){
        Date currentDate = new Date();
        LocalDateTime expiredMinutesLocalDate = LocalDateTime.now().minusMinutes(expirationMinutes);
        Date expiredMinutesDate = Date.from(expiredMinutesLocalDate.
                atZone(ZoneId.systemDefault()).toInstant());
        orderRepository.deleteAllByCreationDateNotBetween(expiredMinutesDate,currentDate);
    }
}
