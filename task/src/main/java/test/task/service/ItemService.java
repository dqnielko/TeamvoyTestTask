package test.task.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import test.task.dto.ItemDto;
import test.task.entity.Item;
import test.task.repository.ItemRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ItemService {
    private final ItemRepository itemRepository;
    public void addItem(ItemDto itemDto){
        Item item = Item.builder().name(itemDto.getName())
                .price(itemDto.getPrice()).build();
        itemRepository.save(item);
    }
    public Optional<Item> findByName(String name){
        return itemRepository.findByName(name);
    }
    @Transactional
    public List<Item> popItemsByNameAndItsLowestPriceLimitedByNumber(String name, int limit){
        Optional<Double> minPriceOptional = itemRepository.findMinPriceWhereName(name);
        List<Item> items = new ArrayList<>();
        if(minPriceOptional.isPresent()){
            double minPrice = minPriceOptional.get();
            items = itemRepository.findItemsByNameAndPriceLimitedByNumber(name,minPrice,limit);
            itemRepository.deleteItemsByNameAndPriceLimitedByNumber(name,minPrice,limit);
        }
        return items;
    }
}
